﻿using System;
using PK.Container;
using Display.Implementation;
using Display.Contract;
using Kontroler.Contract;
using Kontroler.Implementation;


namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IEkran ekran = new Ekran();
            IKontroler kontroler = new Kontroler.Implementation.Kontroler(ekran);
            ContainerG container = new ContainerG();

            container.Register<IEkran>(ekran);
            container.Register<IKontroler>(kontroler);

            return container;
        }
    }
}
