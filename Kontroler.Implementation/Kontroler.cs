﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kontroler.Contract;
using Display.Contract;

namespace Kontroler.Implementation
{
        public class Kontroler : IKontroler
        {
            private IEkran ekranik;

            public Kontroler(IEkran ekranik)
            {
                this.ekranik = ekranik;
            }

            public int ZagrzejKawe()
            {
                return 1;
            }

            public void Programuj()
            {
                Console.WriteLine("Nowa opcja");
            }

            public void ZglosZakonczenie()
            {
                Console.WriteLine("Koniec!");
            }

        }
    }

